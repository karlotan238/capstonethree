const express = require("express");

const TierAssetRouter = express.Router();

const TierAssetModel = require("../models/TierAsset");

TierAssetRouter.post("/addasset", async (req, res) => {
  try {
    let asset = AssetModel({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price
    });

    asset = await asset.save();

    res.send(asset);
  } catch (e) {
    console.log(e);
  }
});

TierAssetRouter.get("/showassets", async (req, res) => {
  try {
    let asset = await TierAssetModel.find();
    res.send(asset);
  } catch (e) {
    console.log(e);
  }
});

TierAssetRouter.get("/showassetbyid/:id", async (req, res) => {
  try {
    let asset = await TierAssetModel.findById(req.params.id);
    res.send(asset);
  } catch (e) {
    console.log(e);
  }
});

TierAssetRouter.put("/updateasset/:id", async (req, res) => {
  try {
    let asset = await TierAssetModel.findById(req.params.id);

    if (!asset) {
      return res.status(404).send(`Asset can't be found`);
    }

    let condition = { _id: req.params.id };
    let updates = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price
    };

    let updatedAsset = await TierAssetModel.findOneAndUpdate(
      condition,
      updates,
      { new: true }
    );

    res.send(updatedAsset);
  } catch (e) {
    console.log(e);
  }
});

TierAssetRouter.delete("/deleteasset/:id", async (req, res) => {
  try {
    let deletedAsset = await TierAssetModel.findByIdAndDelete(req.params.id);
    res.send(deletedAsset);
  } catch (e) {
    console.log(e);
  }
});

module.exports = TierAssetRouter;
