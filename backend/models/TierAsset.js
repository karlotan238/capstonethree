const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TierAssetSchema = new Schema({
  name: {
    type: String,
    default: 'testname'
  },
  description: {
    type: String,
    default: 'testdescription'
  },
  price: {
    type: Number,
    default: 100
  }
});

module.exports = mongoose.model("TierAsset", TierAssetSchema);
