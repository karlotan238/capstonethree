const mogoose = require("mongoose");

const Schema = mongoose.Schema;

const ReceiptSchema = new Schema({
  customerName: String,
  tierName: String,
  bookingDate: String,
  pricePaid: String
});
